import {bootstrap}    from '@angular/platform-browser-dynamic';
import {AppComponent} from './app.component';

import {SocketService} from './sockets/socket.service';
import {ConfigService} from './config/config.service';
import {UsersService} from './users/user.service';

bootstrap(AppComponent, [SocketService, ConfigService, UsersService]);