export class Register {
    nickname: string;
    alias: string;
    password: string;
    rep_password: string;
    email: string;
    rep_email: string;
    birth: string;
    gender: string;

    constructor() {
        this.gender = 'unknown';
        this.birth = '2016-01-01';
    }
}