"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var core_2 = require('@angular/core');
var angular2_recaptcha_1 = require('angular2-recaptcha/angular2-recaptcha');
var socket_service_1 = require('../sockets/socket.service');
var RegisterFormComponent = (function () {
    function RegisterFormComponent(socketService, zone, fb) {
        this.socketService = socketService;
        this.zone = zone;
        this.fb = fb;
        this.submitted = false;
        this.successful = false;
        this.genderOptions = [
            { value: 'unknown', name: 'Sin especificar' },
            { value: 'male', name: 'Hombre' },
            { value: 'female', name: 'Mujer' }
        ];
        this.captcha_resolved = false;
    }
    RegisterFormComponent.prototype.handleCorrectCaptcha = function (event) {
        var _this = this;
        this.zone.run(function () {
            _this.captcha_resolved = true;
        });
    };
    RegisterFormComponent.prototype.onSubmit = function (value) {
        this.error = undefined;
        if (value.rep_email != value.email) {
            this.error = {
                type: 'warning',
                msg: 'Las direcciones de correo electrónico no coinciden.'
            };
        }
        else if (value.rep_password != value.password) {
            this.error = {
                type: 'warning',
                msg: 'Las passwords no coinciden.'
            };
        }
        else if (new Date().getTime() - new Date(value.birth).getTime() < 1000 * 60 * 60 * 24 * 365 * 2) {
            this.error = {
                type: 'warning',
                msg: 'No podemos permitir el registro de recién nacidos; por favor, introduce tu edad real.'
            };
        }
        else {
            this.submitted = true;
            this.socketService.send('request-register', value);
        }
    };
    RegisterFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.form = this.fb.group({
            nickname: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(4)])],
            alias: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(4)])],
            email: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(4)])],
            rep_email: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(4)])],
            password: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(7)])],
            rep_password: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(7)])],
            birth: ['2016-01-01', common_1.Validators.required],
            gender: ['unknown'],
            terms: ['']
        });
        this.socketService.addListener('register', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.submitted = false;
                    if (data.err.code == 101) {
                        _this.error = {
                            type: 'danger',
                            msg: 'El nombre de usuario introducido ya existe. Por favor, escoge uno diferente.'
                        };
                    }
                    else if (data.err.code == 102) {
                        _this.error = {
                            type: 'danger',
                            msg: 'El correo electrónico introducido ya está registrado.'
                        };
                    }
                    else {
                        _this.error = {
                            type: 'danger',
                            msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                        };
                    }
                }
                else {
                    _this.successful = true;
                }
            });
        });
        this.socketService.addListener('session', function (data) {
            if (data.err) {
            }
            else if (data.user) {
                document.location.href = '/';
            }
        });
        this.socketService.send('request-session', { session_token: this.socketService.session_token });
    };
    RegisterFormComponent = __decorate([
        core_1.Component({
            selector: 'amgs-register-form',
            templateUrl: 'app/register/register-form.template.html',
            styleUrls: ['app/register/register-form.style.css'],
            directives: [common_1.FORM_DIRECTIVES, angular2_recaptcha_1.ReCaptchaComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, common_1.FormBuilder])
    ], RegisterFormComponent);
    return RegisterFormComponent;
}());
exports.RegisterFormComponent = RegisterFormComponent;
//# sourceMappingURL=register-form.component.js.map