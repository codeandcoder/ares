import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
    socketUrl = 'http://localhost:4000';
    imagesUploadUrl = 'http://localhost:4000/uploadUserImage';
    imagesUrl = 'http://localhost:4000/images/users';

    constructor() {
    }
}