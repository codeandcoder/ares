import { Component } from '@angular/core';

import { NavComponent } from '../nav/nav.component';
import { MenuItem } from '../nav/menu-item.model';

@Component({
    selector: 'amgs-home',
    template: `
        <amgs-nav [menuItems]="menuItems" [primaryColor]="primaryColor">Load</amgs-nav>
    `,
    directives: [NavComponent]
})
export class HomeComponent {
    menuItems = [
        new MenuItem('Inicio', 'Home', 'fa-home', true),
        new MenuItem('Chat', 'Chat', 'fa-comments-o', false),
        new MenuItem('Foro', 'Forum', 'fa-globe', false)
    ];
    primaryColor = "blue";
}