import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/common';
import { FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/common';
import { NgZone } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { SocketService } from '../sockets/socket.service';

@Component({
  selector: 'amgs-verification',
  templateUrl: 'app/verification/verification.template.html',
  styleUrls: ['app/verification/verification.style.css'],
  providers: [],
  directives: [FORM_DIRECTIVES]
})
export class VerificationComponent implements OnInit {
    error: any;
    successful = false;

    ngOnInit() {
        this.socketService.addListener('verification', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.error = {
                        type: 'danger',
                        msg: 'No se ha podido verificar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                    }
                } else {
                    this.successful = true;
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err) {
            } else if (data.user) {
                document.location.href = '/';
            } else {
                this.socketService.send('request-verification', {
                    token: this.params.get('token')
                });
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token});
    }

    constructor(private socketService: SocketService, private zone: NgZone, private params: RouteParams, private fb: FormBuilder) {
    }
}