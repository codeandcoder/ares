import { Component, Input } from '@angular/core';
import { NgZone } from '@angular/core';

import { SocketService } from '../../sockets/socket.service';
import { Conversation } from '../conversation/conversation.model';
import {UsersService} from '../../users/user.service';

@Component({
    selector: 'amgs-chat-list',
    templateUrl: 'app/chat/list/list.template.html',
    styleUrls: ['app/chat/list/list.style.css'],
    providers: []
})
export class ChatListComponent {
    users = this.usersService.users;
    @Input()
    conversations: Conversation[];

    favorites: any = [];
    rooms: any = [];

    chatUsers: any = {};
    chatUsersList: any[] = [];

    openLists = [true, false, false, true];

    constructor(private socketService: SocketService, private zone: NgZone, private usersService: UsersService) {
        socketService.addListener('chat-users', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.chatUsers = {};
                    this.chatUsersList = [];
                } else {
                    this.chatUsers = data.chatUsers;
                    this.chatUsersList = Object.keys(this.chatUsers);
                    for (var i = 0; i < this.chatUsersList.length; i++) {
                        this.usersService.getUser(this.chatUsersList[i]);
                    }
                }
            });
        });
        socketService.send('request-chat-users', {});
    }
}