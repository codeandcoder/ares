"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var socket_service_1 = require('../../sockets/socket.service');
var user_service_1 = require('../../users/user.service');
var ChatListComponent = (function () {
    function ChatListComponent(socketService, zone, usersService) {
        var _this = this;
        this.socketService = socketService;
        this.zone = zone;
        this.usersService = usersService;
        this.users = this.usersService.users;
        this.favorites = [];
        this.rooms = [];
        this.chatUsers = {};
        this.chatUsersList = [];
        this.openLists = [true, false, false, true];
        socketService.addListener('chat-users', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.chatUsers = {};
                    _this.chatUsersList = [];
                }
                else {
                    _this.chatUsers = data.chatUsers;
                    _this.chatUsersList = Object.keys(_this.chatUsers);
                    for (var i = 0; i < _this.chatUsersList.length; i++) {
                        _this.usersService.getUser(_this.chatUsersList[i]);
                    }
                }
            });
        });
        socketService.send('request-chat-users', {});
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], ChatListComponent.prototype, "conversations", void 0);
    ChatListComponent = __decorate([
        core_1.Component({
            selector: 'amgs-chat-list',
            templateUrl: 'app/chat/list/list.template.html',
            styleUrls: ['app/chat/list/list.style.css'],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, user_service_1.UsersService])
    ], ChatListComponent);
    return ChatListComponent;
}());
exports.ChatListComponent = ChatListComponent;
//# sourceMappingURL=list.component.js.map