import { Component } from '@angular/core';

import { NavComponent } from '../nav/nav.component';
import { MenuItem } from '../nav/menu-item.model';

import { ActiveConversationComponent } from './conversation/active-conversation.component';
import { Conversation } from './conversation/conversation.model';

import { ChatListComponent } from './list/list.component';

@Component({
    selector: 'amgs-chat',
    template: `
        <amgs-nav [menuItems]="menuItems" [primaryColor]="primaryColor">Loading...</amgs-nav>
        <amgs-active-conversation [activeConversation]="activeConversation">Loading...</amgs-active-conversation>
        <amgs-chat-list [conversations]="conversations">Loading...</amgs-chat-list>
    `,
    directives: [NavComponent, ActiveConversationComponent, ChatListComponent]
})
export class ChatComponent {
    menuItems = [
        new MenuItem('Inicio', 'Home', 'fa-home', false),
        new MenuItem('Chat', 'Chat', 'fa-comments-o', true),
        new MenuItem('Foro', 'Forum', 'fa-globe', false)
    ];
    primaryColor = "orange";
    activeConversation: Conversation;
    conversations: Conversation[] = [];
}