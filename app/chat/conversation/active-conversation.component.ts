import { Component, Input } from '@angular/core';

import { Conversation } from './conversation.model';

@Component({
    selector: 'amgs-active-conversation',
    templateUrl: 'app/chat/conversation/active-conversation.template.html',
    styleUrls: ['app/chat/conversation/active-conversation.style.css']
})
export class ActiveConversationComponent {
    @Input()
    conversation: Conversation;
}