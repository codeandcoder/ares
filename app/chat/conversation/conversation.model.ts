import { User } from '../../users/user.model';

export class Conversation {
  title: string;
  user: User;
  open: boolean;

  constructor(title: string, user: User, open: boolean) {
    this.title = title;
    this.user = user;
    this.open = open;
  }
}