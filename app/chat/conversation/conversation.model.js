"use strict";
var Conversation = (function () {
    function Conversation(title, user, open) {
        this.title = title;
        this.user = user;
        this.open = open;
    }
    return Conversation;
}());
exports.Conversation = Conversation;
//# sourceMappingURL=conversation.model.js.map