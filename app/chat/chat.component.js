"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var nav_component_1 = require('../nav/nav.component');
var menu_item_model_1 = require('../nav/menu-item.model');
var active_conversation_component_1 = require('./conversation/active-conversation.component');
var list_component_1 = require('./list/list.component');
var ChatComponent = (function () {
    function ChatComponent() {
        this.menuItems = [
            new menu_item_model_1.MenuItem('Inicio', 'Home', 'fa-home', false),
            new menu_item_model_1.MenuItem('Chat', 'Chat', 'fa-comments-o', true),
            new menu_item_model_1.MenuItem('Foro', 'Forum', 'fa-globe', false)
        ];
        this.primaryColor = "orange";
        this.conversations = [];
    }
    ChatComponent = __decorate([
        core_1.Component({
            selector: 'amgs-chat',
            template: "\n        <amgs-nav [menuItems]=\"menuItems\" [primaryColor]=\"primaryColor\">Loading...</amgs-nav>\n        <amgs-active-conversation [activeConversation]=\"activeConversation\">Loading...</amgs-active-conversation>\n        <amgs-chat-list [conversations]=\"conversations\">Loading...</amgs-chat-list>\n    ",
            directives: [nav_component_1.NavComponent, active_conversation_component_1.ActiveConversationComponent, list_component_1.ChatListComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ChatComponent);
    return ChatComponent;
}());
exports.ChatComponent = ChatComponent;
//# sourceMappingURL=chat.component.js.map