"use strict";
var User = (function () {
    function User(nickname, alias, color, image, confidential, created_at) {
        this.nickname = nickname;
        this.alias = alias;
        this.color = color;
        this.image = image;
        this.confidential = confidential;
        this.created_at = created_at;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.model.js.map