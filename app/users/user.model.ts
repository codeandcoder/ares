export class User {
    nickname: string;
    alias: string;
    color: string;
    image: string;
    confidential: boolean;
    created_at: number;

    constructor(nickname: string, alias: string, color: string, image: string, confidential: boolean, created_at: number) {
        this.nickname = nickname;
        this.alias = alias;
        this.color = color;
        this.image = image;
        this.confidential = confidential;
        this.created_at = created_at;
    }
}