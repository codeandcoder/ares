import { Component } from '@angular/core';

import { NavComponent } from '../nav/nav.component';
import { MenuItem } from '../nav/menu-item.model';

@Component({
    selector: 'amgs-forum',
    template: `
        <amgs-nav [menuItems]="menuItems" [primaryColor]="primaryColor">Load</amgs-nav>
    `,
    directives: [NavComponent]
})
export class ForumComponent {
    menuItems = [
        new MenuItem('Inicio', 'Home', 'fa-home', false),
        new MenuItem('Chat', 'Chat', 'fa-comments-o', false),
        new MenuItem('Foro', 'Forum', 'fa-globe', true)
    ];
    primaryColor = "red";
}