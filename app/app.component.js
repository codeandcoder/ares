"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var login_component_1 = require('./login/login.component');
var register_component_1 = require('./register/register.component');
var verification_component_1 = require('./verification/verification.component');
var home_component_1 = require('./home/home.component');
var terms_component_1 = require('./terms/terms.component');
var recovery_component_1 = require('./recovery/recovery.component');
var rec_component_1 = require('./rec/rec.component');
var chat_component_1 = require('./chat/chat.component');
var user_component_1 = require('./user/user.component');
var forum_component_1 = require('./forum/forum.component');
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        router_deprecated_1.RouteConfig([
            {
                path: '/login',
                name: 'Login',
                component: login_component_1.LoginComponent
            },
            {
                path: '/registro',
                name: 'Register',
                component: register_component_1.RegisterComponent
            },
            {
                path: '/verificar',
                name: 'Verification',
                component: verification_component_1.VerificationComponent
            },
            {
                path: '/',
                name: 'Home',
                component: home_component_1.HomeComponent,
                useAsDefault: true
            },
            {
                path: '/terms',
                name: 'Terms',
                component: terms_component_1.TermsComponent
            },
            {
                path: '/recuperacion',
                name: 'Recovery',
                component: recovery_component_1.RecoveryComponent
            },
            {
                path: '/rec',
                name: 'Rec',
                component: rec_component_1.RecComponent
            },
            {
                path: '/chat',
                name: 'Chat',
                component: chat_component_1.ChatComponent
            },
            {
                path: '/usuario',
                name: 'User',
                component: user_component_1.UserComponent
            },
            {
                path: '/foro',
                name: 'Forum',
                component: forum_component_1.ForumComponent
            }
        ]),
        core_1.Component({
            selector: 'amgs-app',
            template: "\n        <router-outlet></router-outlet>\n    ",
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [
                router_deprecated_1.ROUTER_PROVIDERS
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map