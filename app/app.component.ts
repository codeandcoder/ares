import { Component } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerificationComponent } from './verification/verification.component';
import { HomeComponent } from './home/home.component';
import { TermsComponent } from './terms/terms.component';
import { RecoveryComponent } from './recovery/recovery.component';
import { RecComponent } from './rec/rec.component';
import { ChatComponent } from './chat/chat.component';
import { UserComponent } from './user/user.component';
import { ForumComponent } from './forum/forum.component';

@RouteConfig([
    {
        path: '/login',
        name: 'Login',
        component: LoginComponent
    },
    {
        path: '/registro',
        name: 'Register',
        component: RegisterComponent
    },
    {
        path: '/verificar',
        name: 'Verification',
        component: VerificationComponent
    },
    {
        path: '/',
        name: 'Home',
        component: HomeComponent,
        useAsDefault: true
    },
    {
        path: '/terms',
        name: 'Terms',
        component: TermsComponent
    },
    {
        path: '/recuperacion',
        name: 'Recovery',
        component: RecoveryComponent
    },
    {
        path: '/rec',
        name: 'Rec',
        component: RecComponent
    },
    {
        path: '/chat',
        name: 'Chat',
        component: ChatComponent
    },
    {
        path: '/usuario',
        name: 'User',
        component: UserComponent
    },
    {
        path: '/foro',
        name: 'Forum',
        component: ForumComponent
    }
])

@Component({
    selector: 'amgs-app',
    template: `
        <router-outlet></router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [
        ROUTER_PROVIDERS
    ]
})
export class AppComponent {}