import { Injectable } from '@angular/core';

import { User } from '../users/user.model';
import { ConfigService } from '../config/config.service';

@Injectable()
export class SocketService {
    listeners = {};
    socket: any;
    session_token: string;
    myUser: any;
    recoveringSession = false;

    addListener(key: string, f: Function) {
        if(!this.listeners[key]) {
            this.listeners[key] = [];
        }
        this.listeners[key].push(f);
    }

    send(op: string, data: any) {
        if (op == 'request-session') {
            if (!this.recoveringSession) {
                this.recoveringSession = true;
                this.socket.emit(op, data);
            }
        } else {
            this.socket.emit(op, data);
        }
    }

    private sendToListeners(op: string, data: any) {
        let list = this.listeners[op];
        if (list) {
            for (let i = 0; i < list.length; i++) {
                list[i](data);
            }
        }
    }

    constructor(private config: ConfigService) {
        this.socket = io(config.socketUrl);

        this.session_token = parseCookies(document.cookie).session_token;

        this.socket.on('user-info', (data: any) => {
            let u = data.user;
            if (u) {
                this.sendToListeners('user-info', u);
            }
        });

        this.socket.on('login', (data: any) => {
            this.sendToListeners('login', data);
        });

        this.socket.on('register', (data: any) => {
            this.sendToListeners('register', data);
        });

        this.socket.on('session', (data: any) => {
            this.recoveringSession = false;
            if (data.user) {
                this.myUser = data.user;
                let u = data.user;
                data.user = new User(u.nickname, u.alias, u.color, u.image, u.confidential, u.created_at);
            }
            this.sendToListeners('session', data);
        });

        this.socket.on('recovery', (data: any) => {
            this.sendToListeners('recovery', data);
        });

        this.socket.on('recovery-processed', (data: any) => {
            this.sendToListeners('recovery-processed', data);
        });

        this.socket.on('user-password-updated', (data: any) => {
            this.sendToListeners('user-password-updated', data);
        });

        this.socket.on('user-updated', (data: any) => {
            this.sendToListeners('user-updated', data);
        });

        this.socket.on('verification', (data: any) => {
            this.sendToListeners('verification', data);
        });

        this.socket.on('disconnect', (data: any) => {
            location.reload();
        });

        this.socket.on('logout', (data: any) => {
            location.reload();
        });

        this.socket.on('chat-users', (data: any) => {
            this.sendToListeners('chat-users', data);
        });

        this.socket.on('chat-user', (data: any) => {
            this.sendToListeners('chat-user', data);
        });
    }
}

function parseCookies (rc: string) : any {
    var list = {};

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}