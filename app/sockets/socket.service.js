"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_model_1 = require('../users/user.model');
var config_service_1 = require('../config/config.service');
var SocketService = (function () {
    function SocketService(config) {
        var _this = this;
        this.config = config;
        this.listeners = {};
        this.recoveringSession = false;
        this.socket = io(config.socketUrl);
        this.session_token = parseCookies(document.cookie).session_token;
        this.socket.on('user-info', function (data) {
            var u = data.user;
            if (u) {
                _this.sendToListeners('user-info', u);
            }
        });
        this.socket.on('login', function (data) {
            _this.sendToListeners('login', data);
        });
        this.socket.on('register', function (data) {
            _this.sendToListeners('register', data);
        });
        this.socket.on('session', function (data) {
            _this.recoveringSession = false;
            if (data.user) {
                _this.myUser = data.user;
                var u = data.user;
                data.user = new user_model_1.User(u.nickname, u.alias, u.color, u.image, u.confidential, u.created_at);
            }
            _this.sendToListeners('session', data);
        });
        this.socket.on('recovery', function (data) {
            _this.sendToListeners('recovery', data);
        });
        this.socket.on('recovery-processed', function (data) {
            _this.sendToListeners('recovery-processed', data);
        });
        this.socket.on('user-password-updated', function (data) {
            _this.sendToListeners('user-password-updated', data);
        });
        this.socket.on('user-updated', function (data) {
            _this.sendToListeners('user-updated', data);
        });
        this.socket.on('verification', function (data) {
            _this.sendToListeners('verification', data);
        });
        this.socket.on('disconnect', function (data) {
            location.reload();
        });
        this.socket.on('logout', function (data) {
            location.reload();
        });
        this.socket.on('chat-users', function (data) {
            _this.sendToListeners('chat-users', data);
        });
        this.socket.on('chat-user', function (data) {
            _this.sendToListeners('chat-user', data);
        });
    }
    SocketService.prototype.addListener = function (key, f) {
        if (!this.listeners[key]) {
            this.listeners[key] = [];
        }
        this.listeners[key].push(f);
    };
    SocketService.prototype.send = function (op, data) {
        if (op == 'request-session') {
            if (!this.recoveringSession) {
                this.recoveringSession = true;
                this.socket.emit(op, data);
            }
        }
        else {
            this.socket.emit(op, data);
        }
    };
    SocketService.prototype.sendToListeners = function (op, data) {
        var list = this.listeners[op];
        if (list) {
            for (var i = 0; i < list.length; i++) {
                list[i](data);
            }
        }
    };
    SocketService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [config_service_1.ConfigService])
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
function parseCookies(rc) {
    var list = {};
    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}
//# sourceMappingURL=socket.service.js.map